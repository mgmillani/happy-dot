# Revision history for happy-dot

## 1.0.0.0 -- 2019-12-21

* First version.
  - Parsing of DOT files.
  - Utility function to obtain adjacency of nodes in a DOT file.
  - Utility function to output a DOT file from a list of nodes and edges.
  - Pretty printing.
