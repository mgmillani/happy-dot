module Main where

import Data.List
import Data.Either

import Language.Dot.Parser
import Language.Dot.Graph
import Language.Dot.Utils

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)

g1Str = "graph { v1; v2; v3; v4;}"
g2Str = "graph { v1 -- v2; v3 -- v4;}"
d1Str = "digraph { v1 -> v2; v3 -> v4;}"
g3Str = "graph { v1; v1 -- v2 -- v3; v2 -- {v3 v5 v6} -- {v7 v8 v9}}"
g4Str = "graph { v1 [x = 10]; v2 [x = 5]; v3 [ x = 3, y = 4, z = 1]; v4;}"
g5Str = "graph { v1 -- v2 [color = red]; v3 -- v4 [weight = 5, color = blue];}"

adj1 = [ Node "v1" []
       , Node "v2" []
       , Node "v3" []]
adj2 = [ Edge "v1" "v2" []
       , Edge "v2" "v3" []
       , Edge "v4" "v5" []]
adj3 = [ Node "v1" [(StringID "x", StringID "5")]
       , Node "v2" [(StringID "x", StringID "-1"), (StringID "color", StringID "dark red")]
       ] 
adj4 = [ Edge "v1" "v2" [(StringID "color", StringID "blue")]
       , Edge "v2" "v3" [(StringID "color", StringID "blue"), (StringID "weight", StringID "5")]
       ]

tests = TestList                         
  [ TestLabel "Undirected Graph 1" $ TestCase
    ( do
      let Right ast = parse g1Str
      assertEqual "" ([Node "v1" [], Node "v2" [], Node "v3" [], Node "v4" []], []) (adjacency ast)
    )
  , TestLabel "Undirected Graph 2" $ TestCase
    ( do
      let Right ast = parse g2Str
      assertEqual "" ([ Node "v1" []
                      , Node "v2" []
                      , Node "v3" []
                      , Node "v4" []]
                     ,[ Edge "v1" "v2" []
                      , Edge "v3" "v4" []]) (adjacency ast)
    )
  , TestLabel "Directed Graph 1" $ TestCase
    ( do
      let Right ast = parse d1Str
      assertEqual "" ([ Node "v1" []
                      , Node "v2" []
                      , Node "v3" []
                      , Node "v4" []]
                     ,[ Edge "v1" "v2" []
                      , Edge "v3" "v4" []]) (adjacency ast)
    )
  , TestLabel "Undirected Graph 3" $ TestCase
    ( do
      let Right ast = parse g3Str
      assertEqual "" ([ Node "v1" []
                      , Node "v1" []
                      , Node "v2" []
                      , Node "v3" []
                      , Node "v2" []
                      , Node "v3" []
                      , Node "v5" []
                      , Node "v6" []
                      , Node "v7" []
                      , Node "v8" []
                      , Node "v9" []]
                     ,[ Edge "v1" "v2" []
                      , Edge "v2" "v3" []
                      , Edge "v2" "v3" []
                      , Edge "v2" "v5" []
                      , Edge "v2" "v6" []
                      , Edge "v3" "v7" []
                      , Edge "v3" "v8" []
                      , Edge "v3" "v9" []
                      , Edge "v5" "v7" []
                      , Edge "v5" "v8" []
                      , Edge "v5" "v9" []
                      , Edge "v6" "v7" []
                      , Edge "v6" "v8" []
                      , Edge "v6" "v9" []
                      ]) (adjacency ast)
    )
  , TestLabel "Undirected Graph 4" $ TestCase
    ( do
      let Right ast = parse g4Str
      assertEqual "" ([ Node "v1" [(StringID "x", StringID "10")]
                      , Node "v2" [(StringID "x", StringID "5")]
                      , Node "v3" [(StringID "x", StringID "3")
                                  ,(StringID "y", StringID "4")
                                  ,(StringID "z", StringID "1")]
                      , Node "v4" []]
                     ,[ ]) (adjacency ast)
    )
  , TestLabel "Undirected Graph 5" $ TestCase
    ( do
      let Right ast = parse g5Str
      assertEqual "" ([ Node "v1" []
                      , Node "v2" []
                      , Node "v3" []
                      , Node "v4" []]
                     ,[ Edge "v1" "v2" [(StringID "color", StringID "red")]
                      , Edge "v3" "v4" [(StringID "weight", StringID "5")
                                       ,(StringID "color", StringID "blue") ]
                      ]) (adjacency ast)
    )
  , TestLabel "Output 1" $ TestCase
    ( do
      let dot = adjacencyToDot False (Just "g1") adj1
      assertEqual "" "graph \"g1\" {\n\
                     \ \"v1\";\n\
                     \ \"v2\";\n\
                     \ \"v3\";\n\
                     \}"
                     dot
    )
  , TestLabel "Output 2" $ TestCase
    ( do
      let dot = adjacencyToDot False (Just "g2") adj2
      assertEqual "" "graph \"g2\" {\n\
                     \ \"v1\" -- \"v2\";\n\
                     \ \"v2\" -- \"v3\";\n\
                     \ \"v4\" -- \"v5\";\n\
                     \}"
                     dot
    )
  , TestLabel "Output 3" $ TestCase
    ( do
      let dot = adjacencyToDot False (Just "g3") adj3
      assertEqual "" "graph \"g3\" {\n\
                     \ \"v1\" [\"x\" = \"5\"];\n\
                     \ \"v2\" [\"x\" = \"-1\", \"color\" = \"dark red\"];\n\
                     \}"
                     dot 
    )
  , TestLabel "Output 4" $ TestCase
    ( do
      let dot = adjacencyToDot True (Just "d4") adj4
      assertEqual "" "digraph \"d4\" {\n\
                     \ \"v1\" -> \"v2\" [\"color\" = \"blue\"];\n\
                     \ \"v2\" -> \"v3\" [\"color\" = \"blue\", \"weight\" = \"5\"];\n\
                     \}"
                     dot
    )
  ]


main = do 
  counts <- runTestTT tests
  if errors counts + failures counts > 0 then exitFailure else exitSuccess
