module Main where

import Data.List
import Data.Either

import Language.Dot.Parser
import Language.Dot.Pretty

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)

g1Str = "graph \"g1\" {\n}"
g2Str = "strict graph \"g2\" {\n}"
d1Str = "digraph {\n}"
g3Str = "graph {\n \"v1\";\n \"v2\";\n \"v3\";\n}"
g4Str = "graph {\n \"v1\" -- \"v2\";\n}"
g5Str = "graph {\n\
\ \"v1\" : \"port\" : n;\n\
\ \"v2\" : \"other_port\" [\"attr1\" = \"x\", \"attr2\" = \"y\"];\n\
\ \"v3\" : e;\n\
\}"
g6Str = "graph {\n\
\ \"v1\" -- \"v2\" --\n\
\  {\n\
\   \"rank\" = \"same\";\n\
\   \"v3\";\n\
\   \"v4\";\n\
\  } -- \"v5\" [\"attr1\" = \"5.5\"];\n\
\}"
d2Str = "digraph {\n\
\ \"v1\" -> \"v2\" ->\n\
\  {\n\
\   \"v3\";\n\
\   \"v4\";\n\
\  } ->\n\
\  {\n\
\   \"v5\";\n\
\   \"v6\";\n\
\  };\n\
\}"

tests = TestList                         
  [ TestLabel "Undirected Graph 1" $ TestCase
    ( do
      let Right ast = parse g1Str
      assertEqual "" g1Str (render ast)
    )
  , TestLabel "Undirected Graph 2" $ TestCase
    ( do
      let Right ast = parse g2Str
      assertEqual "" g2Str (render ast)
    )
  , TestLabel "Directed Graph 1" $ TestCase
    ( do
      let Right ast = parse d1Str
      assertEqual "" d1Str (render ast)
    )
  , TestLabel "Undirected Graph 3" $ TestCase
    ( do
      let Right ast = parse g3Str
      assertEqual "" g3Str (render ast)
    )
  , TestLabel "Undirected Graph 4" $ TestCase
    ( do
      let Right ast = parse g4Str
      assertEqual "" g4Str (render ast)
    )
  , TestLabel "Undirected Graph 5" $ TestCase
    ( do
      let Right ast = parse g5Str
      assertEqual "" g5Str (render ast)
    )
  , TestLabel "Undirected Graph 6" $ TestCase
    ( do
      let Right ast = parse g6Str
      assertEqual "" g6Str (render ast)
    )
  , TestLabel "Directed Graph 2" $ TestCase
    ( do
      let Right ast = parse d2Str
      assertEqual "" d2Str (render ast)
    )
  ]

main = do 
  counts <- runTestTT tests
  if errors counts + failures counts > 0 then exitFailure else exitSuccess
