
--------------------------------------------------------------------
-- |
-- Module  : Language.Dot.Graph
-- License : GPL-3
--
-- Maintainer  : Marcelo Garlet Millani <marcelogmillani@gmail.com>
-- Stability   : experimental
-- Portability : portable
--
-- Basic data structures for representing DOT files.
--------------------------------------------------------------------

module Language.Dot.Graph where

data GraphType = Graph | Digraph deriving (Show, Eq)
data Name = StringID String | XMLID String deriving (Eq)
instance Show Name where
  show (StringID str) = str
  show (XMLID str) = str
data Statement =
    EdgeStatement      [Subgraph]            [(Name, Name)]
  | NodeStatement      Name     (Maybe Port) [(Name, Name)]
  | SubgraphStatement  Subgraph
  | AttributeStatement               (Name, Name)
  | EdgeAttribute                   [(Name, Name)]
  | NodeAttribute                   [(Name, Name)]
  | GraphAttribute                  [(Name, Name)]
  deriving (Show, Eq)
data Subgraph = NodeRef Name (Maybe Port) | Subgraph (Maybe Name) [Statement] deriving (Show, Eq)
data Port = Port (Maybe Name) (Maybe Compass) deriving (Show, Eq)
data Compass = North | NorthEast | East | SouthEast | South | SouthWest | West | NorthWest | Center deriving (Show, Eq)

-- | Simple representation of a graph considering only the adjacency of nodes.
data GraphElement = Node String [(Name, Name)] | Edge String String [(Name, Name)] deriving (Eq, Show)
