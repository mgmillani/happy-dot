module GraphGenerator where

import Data.List

generateGrid w h = intercalate "\n" $ concat
  [ [ "graph \"grid-" ++ show (w,h) ++ "\" { "]
  , [ intercalate " -- " [ '"' : show (r,c) ++ "\"" | c <- [1..w] ] | r <- [1..h] ]
  , [ intercalate " -- " [ '"' : show (r,c) ++ "\"" | r <- [1..h] ] | c <- [1..w] ]
  , ["}"]
  ]

generateComplete k = intercalate "\n" $ 
  [ "graph \"complete-" ++ show k ++ "\" { "] ++
  [ show v ++ " [color = blue, x = 20, y = 30, shape = circle];\n" ++ show v ++ " -- {" ++ intercalate " " [show u | u <- [1..v-1]] ++ "}" | v <- [1..k]] ++ ["}"]
  

instances = [generateGrid r r | r <- [50..100]] ++ [generateComplete k | k <- [100..150]]
