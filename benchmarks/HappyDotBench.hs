{-# LANGUAGE BangPatterns #-}
module Main where

import GraphGenerator

import Language.Dot.Parser
import Language.Dot.Utils
import Data.List

import System.Clock
    
runBench !gStr = do
  start <- getTime ProcessCPUTime
  let Right ast = parse gStr
      (_,_,name,_) = ast
      (vertices, edges) = adjacency ast
      !numVerts = length vertices
      !numEdges = length edges
  end <- getTime ProcessCPUTime
  let delta = (toNanoSecs $ diffTimeSpec end start) `div` 10^6 
  return $ ( intercalate ","
              [ "happy-dot"
              , show name
              , show numVerts
              , show numEdges
              , show delta
              ] ,
            delta)

main = do
  results <- mapM runBench instances 
  let times = map snd results
  let avg = sum times
  writeFile "benchmark-happy-dot.csv" $ intercalate "\n" $ "library, instance, vertices, edges, time (ms)" : map fst results
  putStrLn $ "Total time: " ++ show avg ++ " milliseconds"
  
