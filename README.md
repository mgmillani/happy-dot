## happy-dot: A parser for DOT using happy.

This library contains a parser for [DOT](https://www.graphviz.org/doc/info/lang.html) files.
As it uses happy for parsing, it is quite fast (in the benchmarks, it was between three and ten times faster than the `language-dot` library).

